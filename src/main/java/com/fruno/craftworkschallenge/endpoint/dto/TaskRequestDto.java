package com.fruno.craftworkschallenge.endpoint.dto;

import com.fruno.craftworkschallenge.entity.TaskPriority;
import com.fruno.craftworkschallenge.entity.TaskStatus;

import java.time.LocalDateTime;

public class TaskRequestDto {
    private LocalDateTime dueDate;

    private LocalDateTime resolvedAt;

    private String title;

    private String description;

    private TaskPriority priority;
    private TaskStatus status;

    public LocalDateTime getDueDate() {
        return dueDate;
    }

    public void setDueDate(LocalDateTime dueDate) {
        this.dueDate = dueDate;
    }

    public LocalDateTime getResolvedAt() {
        return resolvedAt;
    }

    public void setResolvedAt(LocalDateTime resolvedAt) {
        this.resolvedAt = resolvedAt;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public TaskPriority getPriority() {
        return priority;
    }

    public void setPriority(TaskPriority priority) {
        this.priority = priority;
    }

    public TaskStatus getStatus() {
        return status;
    }

    public void setStatus(TaskStatus status) {
        this.status = status;
    }
}

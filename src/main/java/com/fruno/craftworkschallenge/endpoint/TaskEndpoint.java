package com.fruno.craftworkschallenge.endpoint;

import com.fruno.craftworkschallenge.endpoint.dto.TaskRequestDto;
import com.fruno.craftworkschallenge.endpoint.dto.TaskResponseDto;
import com.fruno.craftworkschallenge.endpoint.mapper.TaskMapper;
import com.fruno.craftworkschallenge.entity.Task;
import com.fruno.craftworkschallenge.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/tasks")
public class TaskEndpoint {
    private final TaskService taskService;
    private final TaskMapper taskMapper;

    @Autowired
    public TaskEndpoint(TaskService taskService, TaskMapper taskMapper) {
        this.taskService = taskService;
        this.taskMapper = taskMapper;
    }

    @GetMapping
    List<TaskResponseDto> getQueue() {
        return taskService.getQueue().stream()
                .map(taskMapper::toResponse)
                .collect(Collectors.toList());
    }

    @GetMapping(value = "/{id}")
    public TaskResponseDto findById(@PathVariable Long id) {
        // I would probably configure some global exception handler in a larger project
        return taskMapper.toResponse(taskService.findById(id));
    }

    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    public TaskResponseDto persist(@RequestBody TaskRequestDto taskDto) {
        Task task = taskService.save(taskMapper.toEntity(taskDto));
        return taskMapper.toResponse(task);
    }

    @PutMapping(value = "/{id}")
    public TaskResponseDto update(@PathVariable Long id, @RequestBody TaskRequestDto taskDto) {
        Task task = taskMapper.toEntity(taskDto);
        task.setId(id);
        return taskMapper.toResponse(taskService.update(task));
    }

    @DeleteMapping(value = "/{id}")
    void deleteById(@PathVariable Long id) {
        taskService.deleteById(id);
    }
}

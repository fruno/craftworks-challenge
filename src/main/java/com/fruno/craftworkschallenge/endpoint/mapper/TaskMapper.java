package com.fruno.craftworkschallenge.endpoint.mapper;

import com.fruno.craftworkschallenge.endpoint.dto.TaskRequestDto;
import com.fruno.craftworkschallenge.endpoint.dto.TaskResponseDto;
import com.fruno.craftworkschallenge.entity.Task;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface TaskMapper {
    TaskResponseDto toResponse(Task task);

    Task toEntity(TaskRequestDto taskRequestDto);
}

package com.fruno.craftworkschallenge.repository;

import com.fruno.craftworkschallenge.entity.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TaskRepository extends JpaRepository<Task, Long> {
    List<Task> findAllByOrderByIdAsc();

    boolean existsById(Long id);

    Optional<Task> findById(Long id);

    Task save(Task task);

    void deleteById(Long id);
}

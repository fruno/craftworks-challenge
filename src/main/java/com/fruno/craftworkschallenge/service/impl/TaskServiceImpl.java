package com.fruno.craftworkschallenge.service.impl;

import com.fruno.craftworkschallenge.Exception.NotFoundException;
import com.fruno.craftworkschallenge.entity.Task;
import com.fruno.craftworkschallenge.entity.TaskPriority;
import com.fruno.craftworkschallenge.repository.TaskRepository;
import com.fruno.craftworkschallenge.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class TaskServiceImpl implements TaskService {

    @Autowired
    private TaskRepository taskRepository;

    @Override
    public List<Task> getQueue() {
        return taskRepository.findAllByOrderByIdAsc();
    }

    @Override
    public Task findById(Long id) {
        return taskRepository.findById(id).orElseThrow(() ->
                new NotFoundException("No task with that id exists."));
    }

    @Override
    public Task save(Task task) {
        return taskRepository.save(task);
    }

    @Override
    public Task update(Task task) {
        if (!taskRepository.existsById(task.getId())) {
            throw new NotFoundException("No task with this id exists.");
        }
        return taskRepository.save(task);
    }

    @Override
    public void deleteById(Long id) {
        try {
            taskRepository.deleteById(id);
        } catch (EmptyResultDataAccessException e) {
            throw new NotFoundException("No task with this id exists.", e);
        }
    }

    @Scheduled(fixedDelay = 15000)
    private void insertTask() {
        Task task = new Task();
        task.setTitle("Automated Task");
        task.setDescription("I shall not bother to generate random data!");
        task.setPriority(TaskPriority.LOW);
        task.setDueDate(LocalDateTime.now().plusDays(30));
        taskRepository.save(task);
    }
}

package com.fruno.craftworkschallenge.service;

import com.fruno.craftworkschallenge.entity.Task;

import java.util.List;

public interface TaskService {

    List<Task> getQueue();

    Task findById(Long id);

    Task save(Task task);

    Task update(Task task);

    void deleteById(Long id);
}

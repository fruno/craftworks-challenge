package com.fruno.craftworkschallenge.entity;

public enum TaskStatus {
    QUEUED,
    RUNNING,
    FAILED,
    COMPLETED
}

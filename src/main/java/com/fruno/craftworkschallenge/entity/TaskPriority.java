package com.fruno.craftworkschallenge.entity;

public enum TaskPriority {
    LOW,
    NORMAL,
    HIGH,
    CRITICAL
}

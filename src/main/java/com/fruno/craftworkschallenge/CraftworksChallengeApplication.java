package com.fruno.craftworkschallenge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CraftworksChallengeApplication {

    public static void main(String[] args) {
        SpringApplication.run(CraftworksChallengeApplication.class, args);
    }

}

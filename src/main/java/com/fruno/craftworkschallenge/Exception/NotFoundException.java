package com.fruno.craftworkschallenge.Exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class NotFoundException extends RuntimeException {
    public NotFoundException(String errorMessage, Throwable err) {
        super(errorMessage, err);
    }

    public NotFoundException(String errorMessage) {
        super(errorMessage);
    }
}
